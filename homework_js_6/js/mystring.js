class MyString extends String{
    static reverse (string){
        let tempArray = string.split("");
        tempArray.reverse();
        string = tempArray.join("");
        return string;
    }

    reverse(){
        let tempArray = this.split("");
        tempArray.reverse();
        return tempArray.join("");
    }

    /*
        Для сравнения сделал две функции. По факту статической пользоваться удобнее: 
            вызываеться функция на классе и в качестве аргумента ей передается строка или переменная содержащая строку, обратно получаем перевернутую строку. 
            Для того чтобы использовать метод который вызывается на экземпляре класса нужно создать экземпляр содержащий нужную строку и принадлежащий нашему классу, затем вызвать на этом экземпляре функцию reverse.
        А главное что статический метод мы можем использовать на любой уже имеющейся строке, совсем не обязательно чтобы она принадлежала нашему классу!
        По сути получается что она работает как просто отдельно написанная функция которую можно применить к чему угодно....
    */

    ucFirst(){
        let str = this.toString();
        return str[0].toUpperCase() + str.slice(1);
    }

    ucWords(string){
        let strArray = string.split(" ");
        string = strArray.map(element => {
            return element[0].toUpperCase() + element.slice(1);
        });
        return string.join(" ");
    }
}

let str = MyString.reverse( prompt("Какую строку будем переворачивать?", "abcde") );
alert(str);

let abc = new MyString( prompt("Какую строку будем переворачивать? Другой способ ))", "qwerty") );
alert( abc.reverse() );

let ucString = new MyString( prompt("Введите строку с маленькой буквы, вернется с большой ))", "например так") );
alert( ucString.ucFirst() );

alert( ucString.ucWords( prompt("Введите строку с маленькой буквы, вернется каждое слово с большой буквы ))", "первая попавшая в голову фраза") ) );

// function rev (string){
//         let tempArray = string.split("");
//         tempArray.reverse();
//         string = tempArray.join("");
//         return string;
// }

// let b = rev("mnbvc");
// alert(b);