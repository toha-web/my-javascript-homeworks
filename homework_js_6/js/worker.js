/*
function Worker(name, surname, days){
    this.name = name;
    this.surname = surname;
    this.rate = 65;
    this.days = days;
    this.getSalary = function(){
        return this.rate * this.days;
    }

    // this.getStatus = function(){
    //     document.write(`Worker ${this.name + " " + this.surname}<br>
    //     Have rate ${this.rate}$<br>
    //     Work ${this.days} days and have salary ${this.getSalary()}<br><br>`);
    // }

    Worker.getStatus = function(obj){
        document.write(`Worker ${obj.name + " " + obj.surname}<br>
        Have rate ${obj.rate}$<br>
        Work ${obj.days} days and have salary ${obj.getSalary()}<br>`);
    }
}
*/


class Worker {
    constructor(name, surname, days){
        this.name = name;
        this.surname = surname;
        this.rate = 65;
        this.days = days;        
    }

    getSalary(){
        return this.rate * this.days;
    }

    getStatus(){
        document.write(`Worker ${this.name + " " + this.surname}<br>
        Have rate ${this.rate}$<br>
        Work ${this.days} days and have salary ${this.getSalary()}<br><br>`);
    }

    static getStatus(obj){
        document.write(`Worker ${obj.name + " " + obj.surname}<br>
        Have rate ${obj.rate}$<br>
        Work ${obj.days} days and have salary ${obj.getSalary()}<br>`);
    }
}

let john = new Worker("John", "Stivenson", 18);

john.getStatus();
Worker.getStatus(john);