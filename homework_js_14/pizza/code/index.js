const pizza = {
    userName: '',
    phone: '',
    email: '',
    size: "",
    topping : [],
    sauce : []
}

const price = {
    size: {
        small: 50,
        mid: 80,
        big: 85
    },
    topping: [
        { price: 20, name: "moc1" },
        { price: 45, name: "moc2" },
        { price: 12, name: "moc3" },
        { price: 93, name: "telya" },
        { price: 78, name: "vetch1" },
        { price: 34, name: "vetch2" },
    ],
    sauce: [
        { price: 60, name: "sauceClassic" },
        { price: 70, name: "sauceBBQ" },
        { price: 50, name: "sauceRikotta" },
    ]
}

window.addEventListener("DOMContentLoaded", () => {
    pizza.size = price.size.big;
    show(pizza);
    document.querySelector("form#pizza")
        .addEventListener("click", (e) => {
            switch (e.target.id) {
                case "small": pizza.size = price.size.small;
                    break;
                case "mid": pizza.size = price.size.mid;
                    break;
                case "big": pizza.size = price.size.big;
                    break;
            }
            show(pizza)
        })
    document.querySelector("#banner")
        .addEventListener("mousemove", (e) => {
            randomPositionBanner(e.target, e.clientX, e.clientY)
        })

    document.querySelector("#banner button")
        .addEventListener("click", () => {
            alert("Ваш промокод : XXXXXX")
        })
})


function randomPositionBanner(banner) {
    const coords = {
        X: Math.floor(Math.random() * document.body.clientWidth),
        Y: Math.floor(Math.random() * document.body.clientHeight)
    }

    const width = (parseInt(getComputedStyle(document.querySelector("#banner"))["width"]) + 100)

    if (coords.X + width > document.body.clientWidth) {
        return
    }

    if (coords.Y + 100 > document.body.clientWidth) {
        return
    }
    // console.log(coords)
    //banner.style.transform = `translateX(300px)`;
    //console.log();
    //document.body.clientWidth / clientHeight

    banner.style.left = coords.X + "px"
    banner.style.top = coords.Y + "px"
}


//валідація 
window.addEventListener('DOMContentLoaded', () => {
    const userName = document.querySelector("[name='name']");
    const userPhone = document.querySelector("[name='phone']");
    const userEmail = document.querySelector("[name='email']");
    const validate = (value, pattern) => pattern.test(value);

    userName.addEventListener('input', () => {
        if (validate(userName.value, /^[а-яіїєґ]{2,}$/i)) {
            userName.classList.add('success');
            userName.classList.remove('error');
            pizza.userName = userName.value;
        }
        else {
            userName.classList.remove('success');
            userName.classList.add('error');
        }
    })


    userPhone.addEventListener('input', () => {
        if (validate(userPhone.value, /^\+380[0-9]{9}$/)) {
            userPhone.classList.add('success');
            userPhone.classList.remove('error');
            pizza.phone = userPhone.value;
        }
        else {
            userPhone.classList.remove('success');
            userPhone.classList.add('error');
        }
    })
    userEmail.addEventListener('change', () => {
        if (validate(userEmail.value, /^[a-z0-9._]{3,40}@[a-z0-9-]{1,777}\.[.a-z]{2,10}$/i)) {
            userEmail.classList.add('success');
            userEmail.classList.remove('error');
            pizza.email = userEmail.value;
        }
        else {
            userEmail.classList.remove('success');
            userEmail.classList.add('error');
        }
    })

});


//Відображення складу
function show(pizza) {
    const price = document.querySelector("#price");
    // topping

    price.innerText = pizza.size
}


//Перетягування.
window.addEventListener("DOMContentLoaded", () => {
    const ingridients = document.querySelector(".ingridients"),
        table = document.querySelector(".table");

    ingridients.addEventListener("dragstart", (e) => {
        //e.target.classList.add("transfer")
        e.dataTransfer.setData("text", e.target.id);
    });

    table.addEventListener("dragenter", () => {
        table.classList.add("transfer")
    })

    table.addEventListener("dragleave", () => {
        table.classList.remove("transfer")
    })

    table.addEventListener("dragover", (e) => {
        e.preventDefault();
        e.stopPropagation();
    })

    table.addEventListener("drop", (e) => {
        e.preventDefault();

        const id = e.dataTransfer.getData("text")

        const img = document.createElement("img");
        img.src = document.getElementById(id).src;
        table.append(img)
        table.classList.remove("transfer")

        getTopping(id);

    })
})

function getTopping(id){
    const part = document.getElementById(id);
    let partType;
    const partTitle = part.nextElementSibling.innerText;

    const priceParts = Object.entries(price);
    priceParts.forEach(pricePart => {
        pricePart.forEach(partEl => {
            if(typeof partEl === "string"){
                partType = partEl;
            }
            else if(Array.isArray(partEl)){
                partEl.forEach(partName => {
                    if(partName.name === id){
                        let counter = 0;
                        pizza.size += partName.price;
                        pizza[partType].push(partTitle);
                        pizza[partType].forEach(el => {
                            if(el === partTitle){
                                counter++;
                            }
                        })

                        const list = document.getElementById(partType);

                        const [...listContent] = list.children;

                        if(listContent.length !== 0){
                            const add = listContent.find(el => {
                                return el.innerText.includes(partTitle);
                            });
                            if(add){
                                add.innerText = `${partTitle} ${counter}`;
                            }
                            else{
                                const div = document.createElement("div");
                                div.innerText = `${partTitle}`;
                                list.append(div);
                                delAdd(div, partTitle, id);
                            }                            
                        }
                        else{
                            const div = document.createElement("div");
                            div.innerText = `${partTitle}`;
                            list.append(div);
                            delAdd(div, partTitle, id);
                        }

                        show(pizza);
                    }
                });
            }
        });
    });
}

function delAdd(add, partTitle, id){
    add.addEventListener("click", () => {
        const partType = add.parentElement.id;
        let counter =  pizza[partType].filter(el => el === partTitle).length;

        if(counter === 1){
            add.remove();
        }
        else if(counter > 1){
            counter--;
            if(counter === 1){
                add.innerText = `${partTitle}`;
            }
            else{
                add.innerText = `${partTitle} ${counter}`;
            }            
        }

        const index = pizza[partType].findIndex(el => el.includes(partTitle));
        if(index >= 0){
            pizza[partType].splice(index, 1);
        }        
        
        const p = price[partType].find(el => el.name === id).price;
        pizza.size -= p;
        show(pizza);

        const [...pizzaImage] = document.querySelectorAll(".table img");
        const [...ingrid] = document.querySelectorAll(".ingridients img");
        const ingridImage = ingrid.find(img => img.id === id).src.split("/").at(-1);

        pizzaImage.filter(el => el.src.includes(ingridImage)).at(-1).remove();
    });
}