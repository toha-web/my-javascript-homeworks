/*Створити конструктор Animal та розширюючі його конструктори Dog, Cat, Horse.
Конструктор Animal містить змінні food, location і методи makeNoise, eat, sleep. Метод makeNoise, наприклад, може виводити на консоль "Така тварина спить".
Dog, Cat, Horse перевизначають методи makeNoise, eat.
Додайте змінні до конструкторів Dog, Cat, Horse, що характеризують лише цих тварин.
Створіть конструктор Ветеринар, у якому визначте метод який виводить в консоль treatAnimal(Animal animal). Нехай цей метод роздруковує food і location тварини, що прийшла на прийом.
У методі main створіть масив типу Animal, в який запишіть тварин всіх типів, що є у вас. У циклі надсилайте їх на прийом до ветеринара. */


function Animal (food, location){
    this.food = food;
    this.location = location;
}
Animal.prototype.makeNoise = function makeNoise (){
    console.log(`${this.name} кричить`);
}
Animal.prototype.eat = function eat (){
    console.log(`${this.name} їсть ${this.food}`);
}
Animal.prototype.sleep = function sleep (){
    console.log(`${this.name} спить`);
}

let animals = [];


function Dog (name, food, wool, location){
    this.name = name;
    this.wool = wool;
    this.sound = "Гав-Гав";
    this.food = food;
    this.location = location;
    animals.push(this);
}
Dog.prototype = Animal.prototype;
Dog.prototype.makeNoise = function makeNoise (){
    console.log(`${this.name} говорить ${this.sound}`);
}
Dog.prototype.eat = function eat (){
    console.log(`${this.name} любить їсти ${this.food}`);
}
let d = new Dog("Jack", "meat", "long", "home");


function Cat (name, food, location, color){
    this.name = name;
    this.sound = "Мяу-Мяу";
    this.food = food;
    this.location = location;
    this.color = color;
    animals.push(this);
}
Cat.prototype = Animal.prototype;
Cat.prototype.makeNoise = function makeNoise (){
    console.log(`${this.name} задовбав своїм ${this.sound}`);
}
Cat.prototype.eat = function eat (){
    console.log(`${this.name} тільки і робить що їсть ${this.food}`);
}
let c = new Cat("Вася", "молоко", "дерево", "рудий");


function Horse (name, food, location, speed){
    this.name = name;
    this.sound = "И-Го-Го";
    this.food = food;
    this.location = location;
    this.speed = speed;
    animals.push(this);
}
Horse.prototype = Animal.prototype;
Horse.prototype.makeNoise = function makeNoise (){
    console.log(`${this.name} робить ${this.sound}`);
}
Horse.prototype.eat = function eat (){
    console.log(`${this.name} після забігу полюбляє ${this.food}`);
}
let h = new Horse("Стрілка", "сіно", "стойло", "100 км/год");



function Vet (){}
Vet.prototype.treatAnimal = function(obj){
    document.write(`Наш пацієнт ${obj.name} полюбляє ${obj.food} та живе в ${obj.location}<br>`);
}
let doc = new Vet();

for(let i = 0; i < animals.length; i++){
    doc.treatAnimal(animals[i]);
}