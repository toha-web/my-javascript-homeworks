class Car{
    constructor(brand = "The Best", weight = "Norm"){
        this.brand = brand;
        this.carClass = this.constructor.name;
        this.weight = weight;
        this.engine = new Engine();
        this.driver = new Driver();
    }
    start(){
        alert("Поїхали!");
    }
    stop(){
        alert("Зупиняємось!");
    }
    turnRight(){
        alert("Поветраємо праворуч!");
    }
    turnLeft(){
        alert("Поветраємо ліворуч!");
    }
    toString(){
        document.getElementById("container").innerHTML += `<div class="item">Наш автомобіль "${this.brand}"<br>
        має клас ${this.carClass} та вагу ${this.weight},<br>
        в ньому стоїть найкращий двигун від ${this.engine.manufacturer} потужністю ${this.engine.power},<br>
        a його водій - неперевершений ${this.driver.fullName} який має досвід ${this.driver.experience}!<br>
        ${this.add || ""}</div><br>`;
    }
}

class Engine{
    constructor(power = "Hulk", manufacturer = "Stark Ind."){
        this.power = power;
        this.manufacturer = manufacturer;
    }
}

class Driver{
    constructor(fullName = "Best Driver", experience = "Level - God"){
        this.fullName = fullName;
        this.experience = experience;
    }
}

class Lorry extends Car{
    constructor(brand, weight, carrying = "Monster"){
        super(brand, weight);
        this.carrying = carrying;
    }
    get add(){
        return `Вантажопідйомність кузова аж ${this.carrying}!!!`;
    }
}

class SportCar extends Car{
    constructor(brand, weight, maxSpeed = "Rocket"){
        super(brand, weight);
        this.maxSpeed = maxSpeed;
    }
    get add(){
        return `Максимальна швидкість ${this.maxSpeed}!!!`;
    }
    
}

let car = new Car("Tesla");
let sc = new SportCar("Ferrari", 1500, 450);
let cat = new Lorry("CAT", "500t", "1500t");


car.toString();
sc.toString();
cat.toString();