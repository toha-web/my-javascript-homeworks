/* Реалізуйте програму перевірки телефону
    * Використовуючи JS Створіть поле для введення телефону та кнопку збереження
    * Користувач повинен ввести номер телефону у форматі 0(Початок з 0)ХХ-ХХХ-ХХ-ХХ
    * Після того як користувач натискає кнопку зберегти перевірте правильність введення номера, якщо номер правильний зробіть зелене тiло і використовуючи document.location переведіть через 3с користувача на сторінку https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg, якщо буде помилка, відобразіть її в діві до input.*/

const phoneInput = document.createElement("input");
phoneInput.placeholder = "0XX-XXX-XX-XX";

const button = document.createElement("button");
button.innerText = "Сохранить";
button.classList.add("phone_btn");

const divError = document.createElement("div");


const pattern = /^0\d\d-\d\d\d-\d\d-\d\d/;

div.appendChild(phoneInput);
div.appendChild(button);

button.onclick = () => {
    if(divError) divError.remove();
    if( pattern.test(phoneInput.value) ){
        button.classList.add("green");
        setTimeout( () => {
            document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
        }, 3000);
    }
    else{
        phoneInput.before(divError);
        divError.classList.add("divError");
        divError.innerText = "Произошла ошибка! Введите номер в нужном формате!";
        phoneInput.value = "";
    }
}