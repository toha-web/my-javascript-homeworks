/* Створіть програму секундомір.
    * Секундомір матиме 3 кнопки "Старт, Стоп, Скидання"<br>
    * При натисканні на кнопку стоп фон секундоміра має бути червоним, старт - зелений, скидання - сірий
    * Виведення лічильників у форматі ЧЧ:ММ:СС<br>
    * Реалізуйте Завдання використовуючи синтаксис ES6 та стрілочні функції*/

const [...timerScreen] = document.querySelectorAll("span");
const [...buttons] = document.querySelectorAll("button");

let startBtn, stopBtn, resetBtn;
let h = 0, m = 0, s = 0;
let timer, timerFlag = false;

buttons.forEach(el => {
    switch(el.textContent){
        case "Start": startBtn = el;
        break;
        case "Stop": stopBtn = el;
        break;
        case "Reset": resetBtn = el;
        break;
    }
});

startBtn.addEventListener("click", () => {
    if(!timerFlag){
        timer = setInterval(() => {
            timerFlag = true;
            s++;
            if(s === 60){
                s = 0;
                m++
                if(m === 60){
                    m = 0;
                    h++;
                }
            }
            timerScreen[2].innerText = s.toString().padStart(2, "0");
            timerScreen[1].innerText = m.toString().padStart(2, "0");
            timerScreen[0].innerText = h.toString().padStart(2, "0");
        }, 1000)
    }
});

stopBtn.addEventListener("click", () => {
    clearInterval(timer);
    timerFlag = false;
});

resetBtn.addEventListener("click", () => {
    clearInterval(timer);
    timerFlag = false;
    h = 0,
    m = 0,
    s = 0;
    timerScreen[2].innerText = s.toString().padStart(2, "0");
    timerScreen[1].innerText = m.toString().padStart(2, "0");
    timerScreen[0].innerText = h.toString().padStart(2, "0");
});