// Створіть на сторінці div і дайте йому зовнішній відступ 150 пікселів. Використовуючи JS виведіть у консоль відступ

const container = document.querySelector(".container");

let div = document.createElement("div");
div.classList.add("div_with_margin");

container.appendChild(div);

let margin = getComputedStyle(div).margin;

console.info(margin);