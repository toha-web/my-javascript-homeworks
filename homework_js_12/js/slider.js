/* Створіть слайдер кожні 3 сек змінюватиме зображення
Зображення для відображення
https://new-science.ru/wp-content/uploads/2020/03/4848-4.jpg
https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg
https://naukatv.ru/upload/files/shutterstock_418733752.jpg
https://cdn.iz.ru/sites/default/files/styles/900x506/public/news-2018-12/20180913_zaa_p138_057.jpg
https://nnst1.gismeteo.ru/images/2020/07/shutterstock_1450308851-640x360.jpg */

const imgContainer = document.querySelector(".slider_img");
const button = document.querySelector(".slider button");
button.textContent = "Start";

const images = ["https://www.shutterstock.com/image-photo/mountain-valley-during-sunrise-natural-600w-287115239.jpg", "https://www.shutterstock.com/image-photo/beautiful-wheat-field-beneath-hill-600w-429276118.jpg", "https://www.shutterstock.com/image-photo/dramatic-sunset-over-mountain-valley-600w-1202695582.jpg", "https://www.shutterstock.com/image-photo/forest-sunset-sky-600w-1112004620.jpg", "https://www.shutterstock.com/image-photo/summer-rural-landscape-sunset-czech-600w-707918587.jpg", "https://www.shutterstock.com/image-photo/green-beautiful-valley-gardens-plantations-600w-2226555173.jpg", "https://www.shutterstock.com/image-photo/highland-mediterranean-travel-landscape-scenic-600w-2224233313.jpg"];

let i = 0;
let isRun = false;
let timer;

const image = document.createElement("img");
image.setAttribute("src", images[i]);
imgContainer.append(image);

button.addEventListener("click", () => {
    if(!isRun){
        i += 1;  
        button.textContent = "Stop";
        timer = setInterval( () => {
            isRun = true;
            if(i < images.length){            
                image.setAttribute("src", images[i++]);                
            }
            else{
                i = 0;
                image.setAttribute("src", images[i++]);
            }            
        }, 3000);
    }
    else{
        clearInterval(timer);
        isRun = false;
        button.textContent = "Start";
    }    
});