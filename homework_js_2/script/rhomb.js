let height = +prompt("Введите высоту ромба", "30");
let width = +prompt("Введите ширину ромба", "15");
let i = 1;

document.write("<style>body{line-height:10px;color:grey;}</style>");

// рисуем расширяющуюся половину ромба
while(i <= (height / 2)){
    for(let j = 0; j < (width - Math.round(i * width / (height / 2))); j++){    // считаем отношение ширины к половине высоты ромба, чтобы узнать ширину пробелов
        document.write("&nbsp;");
    }
    for(let j = 0; j < Math.round(i * width / (height / 2)); j++){  // тоже самое делаем чтобы узнать сколько нужно заполнить звездочками
        document.write("*");
    }
    document.write("<br>");
    i++;
}

// рисуем сужающуюся половину ромба
while(i <= height){
    for(let j = 0; j < (width - Math.round((height - i) * width / (height / 2))); j++){
        document.write("&nbsp;");
    }
    for(let j = 0; j < Math.round((height - i) * width / (height / 2)); j++){
        document.write("*");
    }
    document.write("<br>");
    i++;
}