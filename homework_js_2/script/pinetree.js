let size = parseInt(prompt("Какой высоты елочку будем делать?", "20"));

document.write("<style>body{color: green;line-height:12px}</style>");

for(let i = 0; i < size; i++){
    for(let j = 0; (j + i) < size; j++){
        document.write("&nbsp;");
    }
    for(let j = 0; j < (i + 1); j++){   // при разных символах рисования нужно делать разную длинну строки
        document.write("*");    // например если рисовать буквой "i" то кол-во итераций нужно умножить на 2 (j < (i + 1)*2)
    }
    document.write("<br>");
}

for(let i = 0; i < (size / 5); i++){
    for(let j = 0; j < size; j++){
        document.write("&nbsp;");
    }
    document.write("*<br>");
}