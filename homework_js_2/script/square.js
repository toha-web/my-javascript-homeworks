let size = +prompt("Какая будет ширина квадрата?", "20");
let border = +prompt("Какой ширины будет рамка?", `${size / 10}`);

document.write("<style>body{line-height:8px;color:cornflowerblue;}</style>");

for(let i = 0; i < size; i++){  // запускаем вертикальный цикл
    if(i < border){ // проверяем, если верхняя горизонтальная рамка
        for(let j = 0; j < size; j++){  // то рисуем цельную строку
            document.write("*");
        }
        document.write("<br>");
    }
    else if(i > border && i < size - border){   // если не верхняя и не нижняя рамка
        for(let j = 0; j < border; j++){    // выводим боковую рамку
            document.write("*");
        }
        for(let j = 0; j < (size - border * 2); j++){   // дальше пустоту
            document.write("&nbsp;&nbsp;");
        }
        for(let j = 0; j < border; j++){    // и противоположную боковую рамку
            document.write("*");
        }
        document.write("<br>");
    }
    else if(i >= size - border){    // если нижняя горизонтальная рамка
        for(let j = 0; j < size; j++){  // то рисуем цельную строку
            document.write("*");
        }
        document.write("<br>");
    }    
}