let wallet = {
    owner: "John",
    bitcoin: {
        name: "Bitcoin",
        logo: "<img src='../img/bitcoin.webp' width='30px'>",
        quantity: Math.floor(Math.random() * 10),
        rate: 1016560
    },
    ethereum: {
        name: "Ethereum",
        logo: "<img src='../img/ethereum.webp' width='30px'>",
        quantity: Math.floor(Math.random() * 10),
        rate: 65249
    },
    stellar: {
        name: "Stellar",
        logo: "<img src='../img/Stellar.webp' width='30px'>",
        quantity: Math.floor(Math.random() * 10),
        rate: 3.91
    },

    report: function(){
        let cur = prompt("Щодо якої валюти цікавить інформація? Bitcoin, Ethereum, Stellar");
        cur = cur.toLowerCase();
        // document.write(`Доброго дня, ${this.owner}!<br> На вашому балансі ${this[cur].name + " " + this[cur].logo} залишилося ${this[cur].quantity} монет,<br> якщо ви сьогодні продасте їх те, отримаєте ${this[cur].quantity * this[cur].rate} грн.`);
        if (this[cur]){
            return `Доброго дня, ${this.owner}!<br> На вашому балансі ${this[cur].name + " " + this[cur].logo} залишилося ${this[cur].quantity} монет,<br> якщо ви сьогодні продасте їх те, отримаєте ${this[cur].quantity * this[cur].rate} грн.`;
        }
        else{
            return "У вас немає такої валюти!"
        }
    }
}

document.write( wallet.report() );