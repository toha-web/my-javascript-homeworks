import {createMenu} from "/code/index.js";

const category = document.location.search.substr(1);
document.querySelector("title").innerText = category[0].toLocaleUpperCase() + category.slice(1);

export async function req(url) {
    const data = await fetch(url);
    return data.json();
}

if (category === "characters") {
    req("https://rickandmortyapi.com/api/character")
        .then((data) => {
            showPage(data);
        })
        .catch((e) => { console.error(e) });
}
else if (category === "locations") {
    req("https://rickandmortyapi.com/api/location")
        .then((data) => {
            showPage(data);
        })
        .catch((e) => { console.error(e) });
}
else if (category === "episodes") {
    req("https://rickandmortyapi.com/api/episode")
        .then((data) => {
            showPage(data);
        })
        .catch((e) => { console.error(e) });
}
else{
    console.error("error");
}
console.log("bla-bla-bla");

function showButtons(info){
    const block = document.querySelector(".button-block");
    block.replaceChildren();
    if(info.next){
        const button = document.createElement("button");
        button.className = "np";
        button.innerText = "next";
        block.insertAdjacentElement("beforeend", button);
        button.addEventListener("click", () => {
            req(info.next).then(data => {
                showPage(data);
            });
        });
    }
    if(info.prev){
        const button = document.createElement("button");
        button.className = "np";
        button.innerText = "previous";
        block.insertAdjacentElement("afterbegin", button);
        button.addEventListener("click", () => {
            req(info.prev).then(data => {
                showPage(data);
            });
        });
    }
}

function showPage(e) {
    showButtons(e.info);
    const block = document.querySelector(".cards");
    block.replaceChildren();
    const divs = e.results.map((el) => {
        const div = document.createElement("div");
        div.className = "card"
        const pattern = `<div class="img-card">
        ${el.image ? `<img src="${el.image}" alt="${el.name}">`  : el.type ? el.type : el.air_date}
        </div>
        <h3>${el.name}</h3>
        ${el.species ? `<p>${el.species}</p>` : el.episode ? `<p>${el.episode}</p>`: ""}`;
        div.insertAdjacentHTML("beforeend", pattern);
        div.addEventListener("click", () => {
            openInfo(el);
        });
        return div;
    });
    block.append(...divs);
}

export function openInfo(card){
    const modal = document.querySelector(".box-modal");
    modal.classList.add("active");
    document.querySelector("#modal-closed").addEventListener("click", () => {
        modal.classList.remove("active");
    });
    if(card.url.includes("character")){
        characterInfo(card);
    }
    else if(card.url.includes("location")){
        locationInfo(card);
    }
    else if(card.url.includes("episode")){
        episodeInfo(card);
    }
}

function characterInfo(pers){
    const modal = document.querySelector(".modal-body");
    modal.replaceChildren();

    const fullDate = new Date(pers.created);
    const date = fullDate.toLocaleDateString("default", {weekday: "long", month: "long", day: "numeric", year: "numeric"} );
    const time = fullDate.toLocaleTimeString();

    const avatar = document.createElement("div");
    avatar.className = "avatar";
    avatar.insertAdjacentHTML("afterbegin", `
    <img src="${pers.image}" alt="${pers.name}">
    <h3>${pers.name}</h3>
    `);

    const info = document.createElement("div");
    info.className = "info";
    info.insertAdjacentHTML("afterbegin", `
    <div class="created"><span class="info-title">Дата создания: </span>"${date} ${time}"</div>
    <div class="species"><span class="info-title">Вид: </span>${pers.species}</div>
    <div class="status"><span class="info-title">Статус: </span>${pers.status}</div>
    <div class="gender"><span class="info-title">Пол: </span>${pers.gender}</div>   
    `);

    const divBtnOrigin = document.createElement("div");

    const spanOrigin = document.createElement("span");
    spanOrigin.className = "info-title";
    spanOrigin.innerText = "Откуда родом: ";

    const btnOrigin = document.createElement("button");
    btnOrigin.className = "origin";
    btnOrigin.innerText = `${pers.origin.name}`;
    if(pers.origin.name === "unknown") btnOrigin.disabled = true;
    btnOrigin.addEventListener("click", () => {
        localStorage.setItem("url", `${pers.origin.url}`);
        window.open(`/details/index.html?${pers.name}->${pers.origin.name}`);
        document.querySelector(".box-modal").classList.remove("active");
    });

    divBtnOrigin.append(spanOrigin, btnOrigin);
    info.append(divBtnOrigin);

    const divBtnLoc = document.createElement("div");

    const spanLoc = document.createElement("span");
    spanLoc.className = "info-title";
    spanLoc.innerText = "Последний раз видели: ";

    const btnLoc = document.createElement("button");
    btnLoc.className = "location";
    btnLoc.innerText = `${pers.location.name}`;
    if(pers.location.name === "unknown") btnLoc.disabled = true;
    btnLoc.addEventListener("click", () => {
        localStorage.setItem("url", `${pers.location.url}`);
        window.open(`/details/index.html?${pers.name}->${pers.location.name}`);
        document.querySelector(".box-modal").classList.remove("active");
    });

    divBtnLoc.append(spanLoc, btnLoc);
    info.append(divBtnLoc);

    const divSelect = document.createElement("div");

    const spanSel = document.createElement("span");
    spanSel.className = "info-title";
    spanSel.innerText = "Где засветился: ";

    const select = document.createElement("select");
    select.autocomplete = "off";
    pers.episode.forEach(el => {
        const option = document.createElement("option");
        option.value = el;
        option.innerText = `Episode: ${el.split("/").at(-1)}`;
        option.addEventListener("click", () => {
            localStorage.setItem("url", `${option.value}`);
            window.open(`/details/index.html?${pers.name}->${option.innerText}`);
            document.querySelector(".box-modal").classList.remove("active");
        });
        select.append(option);
    });

    divSelect.append(spanSel, select);
    info.append(divSelect);

    modal.append(avatar, info);
}

function locationInfo(loc){
    const modal = document.querySelector(".modal-body");
    modal.replaceChildren();

    const fullDate = new Date(loc.created);
    const date = fullDate.toLocaleDateString("default", {weekday: "long", month: "long", day: "numeric", year: "numeric"} );
    const time = fullDate.toLocaleTimeString();

    const infoBlock = document.createElement("div");
    infoBlock.className = "info";
    infoBlock.classList.add("location-info");
    infoBlock.insertAdjacentHTML("afterbegin", `
    <h3>${loc.name}</h3>
    <div class="type"><span class="info-title">Тип: </span>${loc.type}</div>
    <div class="created"><span class="info-title">Создана: </span>${date} ${time}</div>
    <div class="dimension"><span class="info-title">Измерение: </span>${loc.dimension}</div>
    `);
    const locRes = document.createElement("div");
    locRes.className = "residents";
    const select = document.createElement("select");
    if(loc.residents.length === 0){
        const option = document.createElement("option");
        option.innerText = "Никого";
        select.disabled = true;
        select.append(option);
    }
    else{
        loc.residents.forEach(el => {
            const option = document.createElement("option");
            option.value = el;
            req(el).then(data => {option.innerText = data.name});
            option.addEventListener("click", () => {
                localStorage.setItem("url", `${option.value}`);
                window.open(`/details/index.html?${loc.name}->Resident`);
                document.querySelector(".box-modal").classList.remove("active");
            });
            select.append(option);
        });
    }
    locRes.append(select);
    select.insertAdjacentHTML("beforebegin", `<span class="info-title">Кого в последний раз тут видели: </span>`);
    infoBlock.append(locRes);

    modal.append(infoBlock);
}

function episodeInfo(episode){
    const modal = document.querySelector(".modal-body");
    modal.replaceChildren();

    const fullDate = new Date(episode.air_date);
    const date = fullDate.toLocaleDateString("default", {weekday: "long", month: "long", day: "numeric", year: "numeric"} );
    const time = fullDate.toLocaleTimeString();

    const infoBlock = document.createElement("div");
    infoBlock.className = "info";
    infoBlock.classList.add("episode-info");
    infoBlock.insertAdjacentHTML("afterbegin", `
    <h3>${episode.name}</h3>
    <div class="type"><span class="info-title">Код: </span>${episode.episode}</div>
    <div class="created"><span class="info-title">Выход в эфир: </span>${date} ${time}</div>
    `);
    const epChar = document.createElement("div");
    epChar.className = "characters";
    const select = document.createElement("select");
    episode.characters.forEach(el => {
        const option = document.createElement("option");
        option.value = el;
        req(el).then(data => {option.innerText = data.name});
        option.addEventListener("click", () => {
            localStorage.setItem("url", `${option.value}`);
            window.open(`/details/index.html?${episode.name}->${option.innerText}`);
            document.querySelector(".box-modal").classList.remove("active");
        });
        select.append(option);
    });
    epChar.append(select);
    select.insertAdjacentHTML("beforebegin", `<span class="info-title">Кто был замечен: </span>`);
    infoBlock.append(epChar);

    modal.append(infoBlock);
}

console.log("bla2-bla2-bla2");