// https://rickandmortyapi.com/api 

async function getData(url, method = "GET") {
    const data = await fetch(url, { method });
    return data.json();
}


getData("https://rickandmortyapi.com/api")
    .then((info) => {       
        const arr = Object.entries(info);
        if (!Array.isArray(arr)) return;
        if(!window.location.search){
            arr.forEach((item) => {
                createCard(item)
            });
        }        
        createMenu(arr);
    });

function createCard(element) {
    const [key] = element;
    const card = document.createElement("div");
    card.innerText = key;
    card.className = "card";
    card.addEventListener("click", () => {
        const wind = window.open( `/page/index.html?${key}`);
    })
    document.querySelector(".cards").append(card);
}

export function createMenu(arr){
    const ul = document.querySelector("ul");
    arr.forEach(el => {
        const li = document.createElement("li");
        const a = document.createElement("a");
        a.innerText = el[0];
        // a.href = el[1];
        a.href = `/page/index.html?${el[0]}`;
        a.target = "_blank";
        li.append(a);
        ul.append(li);
    });
}