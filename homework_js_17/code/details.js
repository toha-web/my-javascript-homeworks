import {createMenu} from "/code/index.js";

// import {req} from "/code/file.js";
const {req} = await import("/code/file.js");

import {openInfo} from "/code/file.js";

const url = localStorage.getItem("url");
localStorage.removeItem("url");

const title = document.querySelector("title");

req(url).then(data => {
    if(url.includes("character")){
        showCharacterPage(data);
        title.innerText = data.name;
    }
    else if(url.includes("location")){
        showLocationPage(data);
        title.innerText = data.name;
    }
    else if(url.includes("episode")){
        showEpisodePage(data);
        title.innerText = data.name;
    }
});

function showCharacterPage(pers){
    genInfo(pers);
    const info = document.querySelector(".detail-info");
    info.insertAdjacentHTML("afterbegin", `
    <div>
        <p><h3>Вид:</h3> ${pers.species}</p>
        <p><h3>Пол:</h3> ${pers.gender}</p>
    </div>
    <div><img src="${pers.image}" alt="${pers.name}"></div>
    <div>
        <p><h3>Дата создания:</h3> ${pers.created}</p>
        <p><h3>Статус:</h3> ${pers.status}</p>
    </div>
    `);
    document.querySelector(".cards").insertAdjacentHTML("beforebegin", "<h3>Где засветился:</h3>");
}

function showLocationPage(loc){
    genInfo(loc);
    const info = document.querySelector(".detail-info");
    info.insertAdjacentHTML("afterbegin", `
    <div>
        <p><h3>Тип планеты:</h3> ${loc.type}</p>
    </div>
    <div><p><h3>Дата создания:</h3> ${loc.created}</p></div>
    <div>        
        <p><h3>Измерение:</h3> ${loc.dimension}</p>
    </div>
    `);
    document.querySelector(".cards").insertAdjacentHTML("beforebegin", "<h3>В последний раз тут видели:</h3>");
}

function showEpisodePage(episode){
    genInfo(episode);
    const info = document.querySelector(".detail-info");
    info.insertAdjacentHTML("afterbegin", `
    <h3>Код серии: ${episode.episode}</h3>
    <h3>Дата выхода: ${episode.air_date}</h3>
    `);
    document.querySelector(".cards").insertAdjacentHTML("beforebegin", "<h3>В ролях:</h3>");
}

function genInfo(data){
    const header = document.querySelector("h2");
    header.innerText = data.name;

    const block = document.querySelector(".cards");
    if(data.characters || data.residents){
        let arr = data.characters || data.residents;
        if(arr.length === 0){
            block.innerHTML = "<h3>Тут никого не было!!!</h3>";
        }
        arr.forEach(el => {
            const div = document.createElement("div");
            div.className = "card"
            req(el).then(pers => {
                const pattern = `<div class="img-card">
                <img src="${pers.image}" alt="${pers.name}"></div>
                <h3>${pers.name}</h3>
                <p>${pers.species}</p>`
                div.insertAdjacentHTML("beforeend", pattern);
                div.addEventListener("click", () => {
                    openInfo(pers);
                });
                block.append(div);
            });
        });
    }
    else if(data.episode){
        data.episode.forEach(el => {
            const div = document.createElement("div");
            div.className = "card"
            req(el).then(episode => {
                const pattern = `
                <h3>${episode.name}</h3>
                <p>${episode.air_date}</p>
                <p>${episode.episode}</p>`
                div.insertAdjacentHTML("beforeend", pattern);
                div.addEventListener("click", () => {
                    openInfo(episode);
                });
                block.append(div);
            });
        });
    }
}