// Створіть багаторядкове поле для введення тексту та кнопки. Після натискання кнопки користувачем програма повинна згенерувати тег div з текстом, який був у багаторядковому полі. багаторядкове поле слід очистити після переміщення інформації

let textarea = document.querySelector("textarea");
let button = document.querySelector(".text_send_btn");

button.onclick = () => {
    let text = textarea.value;
    if(text !== ""){
        let div = document.body.appendChild( document.createElement("div") );
        div.className = "text_div";
        div.innerText = text;
        textarea.value = null;
    }
}

