// Створіть 5 див на сторінці потім використовуючи getElementsByTagName і forEath поміняйте дивам колір фону.

let [...div] = document.getElementsByTagName("div");
let button = document.querySelector("button");

button.onclick = () => {
    div.forEach(element => {
        element.style.backgroundColor = `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`;
    });
}