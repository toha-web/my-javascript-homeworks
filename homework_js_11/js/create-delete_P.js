// Створює на сторінці 10 параграфів і зробіть так, щоб при натисканні на параграф він зникав

let div = document.body.appendChild( document.createElement("div") );
div.className = "wrapper";

let button = div.appendChild( document.createElement("button") );
button.innerText = "Создать параграф";
button.className = "create_button";

button.onclick = () => {
    let p = div.appendChild( document.createElement("p") );
    p.className = "created_P";
    p.innerText = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Odit in quia blanditiis? Laboriosam possimus quod tempora beatae sed magni, esse delectus officiis eveniet et asperiores hic a quo omnis saepe. Ullam doloremque rerum laudantium, voluptatibus ex necessitatibus ab illo consequatur esse reprehenderit quisquam laboriosam accusamus commodi facere facilis perspiciatis recusandae nobis iure aut numquam voluptatum repudiandae eaque, provident nihil. Quos."

    p.onclick = () => {
        p.remove();
    }
}