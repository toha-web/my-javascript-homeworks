/* Створіть картинку та кнопку з назвою "Змінити картинку" 
зробіть так щоб при завантаженні сторінки була картинка
https://itproger.com/img/courses/1476977240.jpg
При натисканні на кнопку вперше картинка замінилася на
https://itproger.com/img/courses/1476977488.jpg
при другому натисканні щоб картинка замінилася на
https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png
*/

const slide = document.querySelector(".image");
const img = slide.appendChild( document.createElement("img") );

let index = 0
const url = ["https://itproger.com/img/courses/1476977240.jpg", "https://itproger.com/img/courses/1476977488.jpg", "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png"]
img.setAttribute("src", url[index]);

const button = document.querySelector(".change_slide");
button.onclick = () => {
    if(index + 1 < url.length){
        index++;
    }
    else{
        index = 0;
    }
    img.setAttribute("src", url[index]);
}