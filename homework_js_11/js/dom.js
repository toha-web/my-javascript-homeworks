// Зробити робочими кнопки з виводом відповідних дій на сторінку приклад 008_DOM

let list = document.getElementById("list");
let listItem = "";
let counter = document.querySelectorAll("ul > li").length;

function selectFirstChild(){
    if(listItem.className === "checked") listItem.classList.remove("checked");
    listItem = list.firstElementChild;
    listItem.className = "checked";     // Не знаю есть ли разница между className = "" и classList.add("")
}
function selectLastChild(){
    if(listItem.className === "checked") listItem.classList.remove("checked");
    listItem = list.lastElementChild;
    listItem.className = "checked";
}
function selectNextNode(){
    if(listItem.className === "checked") listItem.classList.remove("checked");
    if(!listItem){
        listItem = list.firstElementChild;
    }
    else if(listItem === list.lastElementChild){
        listItem = list.firstElementChild;
    }
    else{
        listItem = listItem.nextElementSibling;
    }    
    listItem.className = "checked";
}
function selectPrevNode(){
    if(listItem.className === "checked") listItem.classList.remove("checked");
    if(!listItem){
        listItem = list.lastElementChild;
    }
    else if(listItem === list.firstElementChild){
        listItem = list.lastElementChild;
    }
    else{
        listItem = listItem.previousElementSibling;
    }    
    listItem.className = "checked";
}
function createNewChild(){
    let li = list.appendChild( document.createElement("li") );
    li.innerText = `List Item ${++counter}`;
}
function removeLastChild(){
    if(list.lastElementChild){
        list.lastElementChild.remove();
        if(counter > 0) counter--;
    }    
}
function createNewChildAtStart(){
    let li = document.createElement("li");  // Не понимаю почему в этой функции "li" было undefined пока я не разделил создание
    list.prepend(li);    // и добавление элемента. В функции createNewChild() все работало нормально. А в этой
    li.innerText = "List Item";     // функции элемент создавался а при попытке присвоить li.innerText выдавало li is undefined.
}                                   // При использовании метода list.firstElementChild.before() вело себя точно так же!
