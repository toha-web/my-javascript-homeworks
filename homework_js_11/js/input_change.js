// Створіть 2 інпути та одну кнопку. Зробіть так, щоб інпути обмінювалися вмістом.

let leftInput = document.getElementById("left");
let rightInput = document.getElementById("right");
const button = document.querySelector("[type = button]");

button.onclick = () => {
    let temp = leftInput.value;
    leftInput.value = rightInput.value;
    rightInput.value = temp;
}