// Створити N дівів на сторінці та зробити їх у формі круга, при настиску на дів видаляти його. Дів має мати випадковий колір фону з використанням системи rgb

const button = document.querySelector(".create");

button.onclick = () => {
    const quantity = document.querySelector("#quantity").value;
    document.querySelector("#quantity").value = "";
    const area = document.querySelector(".balls");
    if(quantity < 1 || quantity > 100){
        area.innerText = "Выберите правильное значение!"
    }
    else{
        area.innerText = "";
        for(let i = 1; i <= quantity; i++){
            let ball = document.createElement("div");
            function randomColor (){
                return Math.floor( Math.random() * 255 );
            }
            ball.style.backgroundColor = `rgb( ${randomColor()}, ${randomColor()}, ${randomColor()} )`;
            ball.className = "ball";
            ball.innerText = i;
            area.appendChild(ball);

            ball.onclick = () => {
                ball.remove();
            }
        }
    }
}