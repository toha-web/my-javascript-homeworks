/*
Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].
*/


const myArray = [123, "Hello, world!", 456, "JavaScript", 789, true, -123, "Web Development", 0, "Programming", 1.23, "Data Science", undefined, "Artificial Intelligence", 999, "Machine Learning", 0.5, "Software Engineering", false, "Computer Science", 100000, "Frontend Development", -0.5, true, 1e6, "Full Stack Development", undefined, "Mobile Development", 42, false];

document.write(`<strong>Массив до фильтрации<br></strong>`);
myArray.forEach(el => {
    document.write(`${el} - ${typeof(el)}<br>`);
});

function filterBy(array, type){
    return array.filter(el => typeof(el) !== type);
}

let type = prompt("Какой тип данных будем отфильтровывать? 'string', 'number', 'boolean', 'undefined'");
let modArray = filterBy(myArray, type);

document.write(`<br><strong>Массив после удаления элементов типа ${type}<br></strong>`);
modArray.forEach(el => {
    document.write(`${el} - ${typeof(el)}<br>`);
});