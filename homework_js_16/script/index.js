/* Парсинг данный из таблицы Википедии "https://uk.wikipedia.org/wiki/%D0%9A%D0%BB%D0%B0%D1%81%D0%B8%D1%84%D1%96%D0%BA%D0%B0%D1%86%D1%96%D1%8F_%D0%B2%D0%B0%D0%BB%D1%8E%D1%82_(ISO_4217)"

Находим таблицу
const table = document.querySelector("tbody");

Создаем класс для каждой валюты
class currencyCode{
    constructor(country, currency, litCode, numCode){
        this.country = country;
        this.currency = currency;
        this.litCode = litCode;
        this.numCode = numCode;
    }    
}

Создаем массив для хранения объектов
let codes = [];

Пробегаем по таблице и выдергиваем нужные данные из ячеек, тут же создаем экземпляр класса и добавляем его в массив
for(let i = 0; i < table.children.length; i++){
    codes.push(new currencyCode(table.children[i].children[0].innerText, table.children[i].children[1].innerText, table.children[i].children[2].innerText, table.children[i].children[3].innerText));
}

Когда массив со всеми кодами готов делаем из него JSON и копируем данные в файл
const jsonCodes = JSON.stringify(codes);
*/

/*
(function getCodes(){
    let request = new XMLHttpRequest;
    request.open("GET", "./script/currencyCodes.json");
    request.send();
    request.addEventListener("loadend", () => {        
        if(request.readyState === 4 && request.status >=200 && request.status < 300){
            show(JSON.parse(request.response));
        }
        else{
            console.error(request.statusText);
        }
    })
})();

function getCurrExch(){
    return new Promise((resolve, reject) => {
        let request = new XMLHttpRequest;
        request.open("GET", "https://api.monobank.ua/bank/currency");
        request.send();
        request.addEventListener("loadend", () => {
            if(request.readyState === 4 && request.status >=200 && request.status < 300){
                resolve(JSON.parse(request.response));
            }
            else{
                reject("Словили ошибку");
            }
        })
    });
}

getCurrExch()
.then(rate => {
    rate.forEach(code => {
        console.log(code);
    });
})
.catch(error => console.log(error));

function show(arr){
    arr.forEach(code => {
        console.log(code);
    });
}
*/

// ---- Currency Exchange ----- //

const getCodes = fetch("./script/currencyCodes.json").then(data => data.json());
const getCurrency = fetch("https://api.monobank.ua/bank/currency").then(data => data.json());

Promise.all([getCodes, getCurrency])
    .then(el => {
        let codes = [];
        let currency = [];
        el.forEach(arr => {
            if(Object.keys(arr[0]).includes("country")){
                codes = arr;
            }
            else if(Object.keys(arr[0]).includes("currencyCodeA")){
                currency = arr;
            }
        });
        createCurrencyExchange(currency, codes);
    })
    .catch(error => console.error(error));

function createCurrencyExchange(currency, codes){
    currency.forEach(obj => {
        const curr1 = codes.find(el => el.numCode == obj.currencyCodeA);
        const curr2 = codes.find(el => el.numCode == obj.currencyCodeB);
        if(curr1 && curr2){
            addToTable(curr1, curr2, obj);
        }        
    });
}

function addToTable(curr1, curr2, obj){
    const table = document.querySelector("#currency tbody");
    const row = document.createElement("tr");
    row.insertAdjacentHTML("beforeend",
    `
        <td>${curr1.currency} - ${curr1.litCode}</td>
        <td>${curr2.currency} - ${curr2.litCode}</td>
        <td>${obj.date}</td>
        <td>${obj.rateBuy}</td>
        <td>${obj.rateSell}</td>
        <td>${obj.rateCross}</td>
    `);
    table.append(row);
}


// ----- Nova Poshta ---- //
/*
const url = "https://api.novaposhta.ua/v2.0/json/";
const options = {
	"apiKey": "187a43353fe2c3faa260fc282c7c2f06",
	"modelName": "Address",
	"calledMethod": "getCities",
    "methodProperties": {
        "BicycleParking": "1",
        "TypeOfWarehouseRef":"9a68df70-0267-42a8-bb5c-37f427e36ee4",
        "PostFinance": "1",
        "CityName":"Київ",
        "CityRef": "20982d74-9b6c-11e2-a57a-d4ae527baec3",
        "WarehouseId": "151",
        "FindByString": "Відділення №8",
        "Ref": "47402ec5-e1c2-11e3-8c4a-0050568002cf",
        "SettlementRef": "e71629ab-4b33-11e4-ab6d-005056801329"
    }
};
let response = await fetch(url, options);
let data = await response.json();
console.log(data);
*/
fetch("https://api.novaposhta.ua/v2.0/json/", {
	"apiKey": "187a43353fe2c3faa260fc282c7c2f06",
	"modelName": "Address",
	"calledMethod": "getWarehouses",
    "methodProperties": {}
})
//  .then(data => console.log(data))
.then(data => data.json())
.then(val => console.log(val))


// -----Weather in Kyiv ----- //
async function getCurrentWeather(){
    await fetch("https://api.foreca.net/data/recent/100703448.json")
    .then(data => data.json())
    .then(value => {
        showCurrentWeather(value[100703448]);
    })
    .catch(error => {
        if(error){
            console.log(error);
        }
        else{
            console.log("Перезагрузите страницу");
        }
    })
}

function showCurrentWeather(weather){
    const weatherBlock = document.querySelector(".weather-block-now");
    const date = new Date(weather.time);
    const dateDay = date.toLocaleDateString("uk-UA", {weekday: "long", month: "long", day: "2-digit"});
    const dateTime = date.toLocaleTimeString();
    const windD = Math.round(weather.windd / 45) * 45;
    weatherBlock.insertAdjacentHTML("afterbegin", `
    <div class="title-city">Київ</div>
    <div class="title-date">Оновлено: ${dateDay} ${dateTime}</div>
    <div class="day-temp">
        <div class="info">
            <span class="temp">Температура: +${weather.temp}</span>
            <span class="feel">Відчувається: +${weather.flike}</span>
        </div>
        <div class="icon">
            <img src="https://www.foreca.com/public/images/symbols/${weather.symb}.svg">
        </div>
    </div>
    <div class="wind">
        <div class="icon">
            <img src="https://www.foreca.com/public/images/wind/blue/w${windD}.svg">
        </div>
        <div class="info">
            <span class="speed">Вітер: ${weather.winds}м/с</span>
            <span class="max-speed">Пориви вітру: ${weather.maxwind}м/с</span>
        </div>
    </div>
    <div class="other">
        <span class="preasure">Атмосферний тиск: ${Math.round(weather.pres)}hPa</span>
        <span class="rhum">Відносна вологість: ${weather.rhum}%</span>
    </div>`);
}
getCurrentWeather();

const getTomorrowWeather = fetch("https://api.foreca.net/data/favorites/100703448.json");
getTomorrowWeather.then(data => data.json())
                    .then(value => {
                        const updatedTime = value[100703448][0].updated;
                        if(new Date(updatedTime).getHours() < 18){
                            showTomorrowWeather(value[100703448][0]);
                        }
                        else{
                            showTomorrowWeather(value[100703448][1]);
                        }
                    })
                    .catch(error => {
                        if(error){
                            console.log(error);
                        }
                        else{
                            console.log("Перезагрузите страницу");
                        }
                    })

function showTomorrowWeather(weather){
    const weatherBlock = document.querySelector(".weather-block-tomorrow");
    const date = new Date(weather.date);
    const dateDay = date.toLocaleDateString("uk-UA", {weekday: "long", month: "long", day: "2-digit"});
    const windD = Math.round(weather.windd / 45) * 45;
    weatherBlock.insertAdjacentHTML("afterbegin", `
    <div class="title-date">${dateDay}</div>
    <div class="day-temp">
        <div class="info">
            <span class="temp-max">Максимальна температура: +${weather.tmax}</span>
            <span class="temp-min">Мінімальна температура: +${weather.tmin}</span>
        </div>
        <div class="icon">
            <img src="https://www.foreca.com/public/images/symbols/${weather.symb}.svg">
        </div>
    </div>
    <div class="wind">
        <div class="icon">
            <img src="https://www.foreca.com/public/images/wind/blue/w${windD}.svg">
        </div>
        <div class="info">
            <span class="speed">Вітер: ${weather.winds}м/с</span>
        </div>
    </div>
    <div class="other">
        <span class="rain">Опади: ${weather.rain}mm</span>
        <span class="rain-p">Вірогідність опадів: ${weather.rainp}%</span>
        <span class="sunrise">Схід сонця: ${weather.sunrise}</span><span class="sunset">Захід сонця: ${weather.sunset}</span>
    </div>`);
}