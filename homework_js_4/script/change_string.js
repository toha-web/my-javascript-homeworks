let sourceString = "var_text_hello";

//tempArray = Array.from(sourceString); // Не лучший вариант, массив в итоге состоит из отдельных символов, которые нужно перебирать циклом, искать разделитель и уже потом проводить все остальные действия


function changeString(str){
    let tempArray = str.split("_"); // Этот метод сразу делит строку по разделителю и создает массив состоящий из отдельных слов

    document.write(`Исходная строка: "${str}"<br>`);
    document.write(`Массив из элементов строки разделенных "_":  ${tempArray.join(", ")}<br>`);

    let resultString = "";
    
    for(let item of tempArray){
        resultString += (item.toString()[0].toUpperCase() + item.toString().slice(1));
    }
    return resultString;
}

document.write(`<p>Результат: ${changeString(sourceString)}</p>`);