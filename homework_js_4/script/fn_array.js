let sourceArray = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

alert("Данная функция сдвигает элементы массива на N позиций");

function getSteps(){
    return +prompt("На сколько шагов сдвинем элементы массива?", "1");
}

let steps = getSteps();

function moveArrayItems(array){
    let res = [];
    if(steps < array.length){
        for(let i = 0; i < array.length - steps; i++){
            res[steps + i] = sourceArray[i];
        }
        for(let i = 0; i < steps; i++){
            res[i] = sourceArray[array.length - steps + i];        
        }
        return res;
    }
    else{
        alert("Колличество шагов больше чем длинна массива!!!");
    }
}

function map(array, fn){
    return fn(array);
}

let resultArray = map(sourceArray, moveArrayItems);
if(resultArray){
    document.write(`<p>Исходный массив ${sourceArray.join(" ")}</p><br>
    <p>Мы делаем сдвиг на ${steps} шагов</p><br>
    <p>В итоге получаем массив ${resultArray.join(" ")}</p>`);
}
else{
    document.write("Вы ввели не корректные данные, обновите страницу и попробуйте еще раз!");
}