let x, y, operator;

function getNumbers(){
    let z;
    do{
        z = +prompt("Введите первое число", "0");
    }
    while(!/\d/g.test(z));
    x = z;
    do{
        z = +prompt("Введите второе число", "0");
    }
    while(!/\d/g.test(z));
    y = z;
}

function getOperator(){
    let z;
    do{
        z = prompt("Какую операцию будем производить?  Выберите  + , - , * , /", "");
    }
    while(!/\+|\-|\*|\//.test(z));
    operator = z;
}

function calc(){
    getNumbers();
    getOperator();
    let res;
    switch(operator){
        case "+":
            res = x + y;
        break;
        case "-":
            res = x - y;
        break;
        case "*":
            res = x * y;
        break;
        case "/":
            if(y === 0){
                alert("Деление на 0 запрещено!!!");
                return;
            }
            else{
                res = x / y;
            }
        break;
    }
    alert(`${x} ${operator} ${y} = ${res}`);
}

calc();