/*
* У папці calculator дана верстка макета калькулятора. 
Потрібно зробити цей калькулятор робочим.
* При натисканні на клавіші з цифрами - набір введених цифр має бути показаний на табло калькулятора.
* При натисканні на знаки операторів (`*`, `/`, `+`, `-`) на табло нічого не відбувається - програма чекає введення другого числа для виконання операції.
* Якщо користувач ввів одне число, вибрав оператор і ввів друге число, то при натисканні як кнопки `=`, так і будь-якого з операторів, в табло повинен з'явитися результат виконання попереднього виразу.
* При натисканні клавіш `M+` або `M-` у лівій частині табло необхідно показати маленьку букву `m` - це означає, що в пам'яті зберігається число. Натискання на MRC покаже число з пам'яті на екрані. Повторне натискання `MRC` має очищати пам'ять.
*/

const keys = document.querySelector(".keys");
const display = document.querySelector(".display input");
const btnEq = document.querySelector("input[value = '=']");
const span = document.querySelector(".display span");
let calcStatus = false, flag = false;
let firstOperand, secondOperand, operator, result, memory;
const numPattern = /[\d\.]/;
const operPattern = /^[\+\-\*\/]$/;

function getValue(key, event){
    if(!flag) display.value = "0";
    flag = true;
    if(key === "." && display.value.includes(".")){
        event.preventDefault();
    }
    else if(operPattern.test(key) || key === "="){
        if(display.value.includes(".")){
            flag = false;
            return parseFloat(display.value);
        }
        else if(!display.value.includes(".")){
            flag = false;
            return parseInt(display.value);
        }
    }
    else{
        if(display.value.startsWith("0") && key !== "." && display.value.length > 0 && display.value[1] !== "."){
            display.value = display.value.slice(1);
            if(display.value.length < 16){
                display.value += key;
            }
        }
        else if(display.value.startsWith(".")){
            display.value = display.value.padStart(2, "0");
            if(display.value.length < 16){
                display.value += key;
            }
        }
        else{
            if(display.value.length < 16){
                display.value += key;
            }
        }
    }
}
function calc(a, b, operator){
    switch(operator){
        case "+": return a + b;
        break;
        case "-": return a - b;
        break;
        case "*": return a * b;
        break;
        case "/": return b !== 0 ? a / b : "Error";
        break;
    }
}
function reset(){
    display.value = "0";
    firstOperand = undefined;
    secondOperand = undefined;
    operator = undefined;
    result = undefined;
    btnEq.disabled = true;
}


keys.addEventListener("click", (event) => {
    let key = event.target.value;
    if(!calcStatus && !numPattern.test(key)){
        calcStatus = true;
        display.value = "0";
    }
    else if(display.value === "Error"){
        reset();
    }
    else if(numPattern.test(key)){
        calcStatus = true;
        if(firstOperand || firstOperand === 0){
            btnEq.disabled = false;
        }
        getValue(key, event);
    }
    else if(calcStatus && operPattern.test(key)){
        if(!firstOperand && firstOperand !== 0){
            firstOperand = getValue(key, event);
            operator = key;
        }
        else if(!secondOperand && secondOperand !== 0 && operator){
            secondOperand = getValue(key, event);
            result = calc(firstOperand, secondOperand, operator);
            console.log(`${firstOperand} ${operator} ${secondOperand} = ${result}`);
            display.value = result;
            operator = key;
            firstOperand = result;
            secondOperand = undefined;
            result = undefined;
        }
        else if(firstOperand && !operator){
            operator = key;
            flag = false;
        }
        else if(firstOperand && secondOperand){
            result = calc(firstOperand, secondOperand, operator);
            console.log(`${firstOperand} ${operator} ${secondOperand} = ${result}`);
            display.value = result;
            operator = key;
            firstOperand = result;
            secondOperand = undefined;
            result = undefined;
            flag = false;
        }
    }
    else if(key === "C"){
        reset();
    }
    else if(key === "="){
        if(firstOperand || firstOperand === 0){
            if(!operator){
                event.preventDefault();
            }
            else{
                secondOperand = getValue(key, event);
                result = calc(firstOperand, secondOperand, operator);
                console.log(`${firstOperand} ${operator} ${secondOperand} = ${result}`);
                display.value = result;
                operator = undefined;
                firstOperand = result;
                secondOperand = undefined;
                result = undefined;
            }
        }
    }
    else if(key === "m+"){
        if(display.value !== "0"){
            memory = display.value;
        }
        else{
            event.preventDefault();
        }
    }
    else if(key === "m-"){
        if(memory){
            memory = undefined;
        }
        else{
            event.preventDefault();
        }
    }
    else if(key === "mrc"){
        if(memory){
            if(!firstOperand){
                firstOperand = +memory;
                getValue(key, event);                
            }
            else if(firstOperand && !secondOperand){
                secondOperand = +memory;
                getValue(key, event);
            }
            display.value = memory;
        }
        else{
            event.preventDefault();
        }
    }
    if(memory){
        span.classList.remove("hidden");
    }
    else if(!memory){
        span.classList.add("hidden");
    }
    console.log(`${firstOperand} ${operator} ${secondOperand} = ${result}`);
});