function showComment(comm){
    const comments = document.querySelector(".comments");
    const comment = document.createElement("div");
    comment.className = "comment";
    comment.insertAdjacentHTML("afterbegin", 
    `<div class="comment-head">
        <div class="comment-avatar">
            <img src="${comm.image}" alt="${comm.name}">
        </div>
        <div class="comment-name">${comm.name}</div>
        <div class="comment-id">${comm.id}</div>
    </div>
    <div class="comment-body">${comm.body}</div>
    <div class="comment-footer">
        <div class="comment-email">
            <a href="mailto:${comm.email}">${comm.email}</a>
        </div>
    </div>`);
    comments.append(comment);
}

function pageButtons(comments, avatars){
    const pageBlock = document.querySelector(".pagination");
    for(let i = 0; i < (comments.length / 100 + 2); i++){
        const button = document.createElement("div");
        button.className = "p-button";
        if(i === 0){
            button.classList.add("prev");
            button.innerText = "<";
            button.style.display = "none";
        }
        else if(i === 1){
            button.innerText = i;
            button.classList.add("active");
        }
        else if(i === (comments.length / 100 + 1)){
            button.classList.add("next");
            button.innerText = ">";
        }
        else{
            button.innerText = i;
        }
        button.addEventListener("click", (event) => {
            let currentBtn;
            for(let i = 1; i < pageBlock.children.length - 1; i++){
                if(pageBlock.children[i].classList.contains("active")){
                    currentBtn = pageBlock.children[i];
                }
            }
            if(event.target.innerText !== "<" && event.target.innerText !== ">"){
                currentBtn.classList.remove("active");
                event.target.classList.add("active");
                currentBtn = event.target;
            }
            else if(event.target.innerText === "<" && +currentBtn.innerText !== 1){
                currentBtn.classList.remove("active");
                currentBtn.previousElementSibling.classList.add("active");
                currentBtn = currentBtn.previousElementSibling;
            }
            else if(event.target.innerText === ">" && +currentBtn.innerText !== pageBlock.children.length - 2){
                currentBtn.classList.remove("active");
                currentBtn.nextElementSibling.classList.add("active");
                currentBtn = currentBtn.nextElementSibling;
            }
            if(+currentBtn.innerText > 1){
                document.querySelector(".prev").style.display = "flex";
                if(+currentBtn.innerText === pageBlock.children.length - 2){
                    document.querySelector(".next").style.display = "none";
                }
                else if(+currentBtn.innerText < pageBlock.children.length - 2){
                    document.querySelector(".next").style.display = "flex";
                }
            }
            else if(+currentBtn.innerText === 1){
                document.querySelector(".prev").style.display = "none";
                document.querySelector(".next").style.display = "flex";
            }
            page(+currentBtn.innerText, comments, avatars);
        });
        pageBlock.append(button);
    }
    page(1, comments, avatars);
}

function page(num, comments, avatars){
    window.scrollTo(0, 0);
    let fel = num + "00";
    let count = 0;
    document.querySelector(".comments").replaceChildren();
    for(let i = fel - 100; i < fel; i++){
        comments[i].image = avatars.photos[count].url;
        showComment(comments[i]);
        count++;
    }
}

function getAvatars(comments){
    const getAvatar = new XMLHttpRequest;
    getAvatar.open("GET", "https://api.slingacademy.com/v1/sample-data/photos?limit=100");
    getAvatar.send();
    getAvatar.addEventListener("readystatechange", () => {
        if(getAvatar.readyState === 4 && getAvatar.status >= 200 && getAvatar.status < 300){
            const avatars = (JSON.parse(getAvatar.response));
            pageButtons(comments, avatars);
        }
    });
}

const getComments = new XMLHttpRequest;
getComments.open("GET", "https://jsonplaceholder.typicode.com/comments");
getComments.send();
getComments.addEventListener("readystatechange", () => {
    if(getComments.readyState === 4 && getComments.status >= 200 && getComments.status < 300){        
        const comments = JSON.parse(getComments.response);
        getAvatars(comments);
    }
});
