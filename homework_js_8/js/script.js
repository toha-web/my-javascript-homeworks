const request = new XMLHttpRequest();
request.open('GET', "./js/products.json");
request.responseType = 'json';
request.send();
request.onload = function(){
    const products = request.response;
    createProductArray(products);
}

class Product{
    constructor(name, img, price, desc){
        this.name = name;
        this.img = img;
        this.productPrice = price;
        this.productDescription = desc;
    }
}

function createProductArray(jsonarray){
    let products = [];
    jsonarray.forEach(function(element){        
        products.push(new Product(element.title, element.image, element.price, element.description));
    });

    products.forEach(function(element){
        createCards(element);
    });
}

function createCards(products){
    document.getElementById("row").innerHTML += `
    <div class="col-12 col-md-6 col-lg-4 col-xl-3">
        <div class="card">
            <img src="${products.img}" class="card-img-top" alt="${products.name}">
            <div class="card-body">
                <h5 class="card-title">${products.name}</h5>
                <p class="card-text">${products.productDescription}</p>
                <a href="#" class="btn btn-primary">${products.productPrice}</a>
            </div>
        </div>
    </div>`;
}